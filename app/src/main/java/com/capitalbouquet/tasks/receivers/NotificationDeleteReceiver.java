package com.capitalbouquet.tasks.receivers;


import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.capitalbouquet.tasks.util.NotificationUtil;

public class NotificationDeleteReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {
        NotificationUtil.updateMessagesCount(0);
    }
}
