package com.capitalbouquet.tasks.ui.main.tasks.adapter;


import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.capitalbouquet.tasks.R;
import com.capitalbouquet.tasks.data.model.tasks.Task;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TaskAdapter extends RecyclerView.Adapter<TaskAdapter.ViewHolder> {

    private List<Task> mTasks;
    private OnItemClickListener mCallback;

    public interface OnItemClickListener {
        void onItemClick(Task task);
    }

    public TaskAdapter(List<Task> tasks, OnItemClickListener callback) {
        mTasks = tasks != null ? tasks : new ArrayList<>();
        mCallback = callback;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.task_item, parent, false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        Task task = mTasks.get(position);
        Context context = holder.itemView.getContext();
        holder.mId.setText("#" + task.getId());
        switch (task.getPriority()) {
            case 1:
                holder.mId.setTextColor(ContextCompat.getColor(context, R.color.priority_low));
                break;
            case 2:
                holder.mId.setTextColor(ContextCompat.getColor(context, R.color.priority_middle));
                break;
            case 3:
                holder.mId.setTextColor(ContextCompat.getColor(context, R.color.priority_high));
                break;
            case 4:
                holder.mId.setTextColor(ContextCompat.getColor(context, R.color.priority_ev));
                break;
        }
        holder.mText.setText(task.getMultiTaskFull());
        int period = 0;
        switch (task.getStatus()) {
            case 1:
                period = task.getPeriod();
                holder.mType.setImageResource(R.drawable.ic_hourglass_empty_red_300_24dp);
                break;
            case 2:
                period = task.getPeriod();
                holder.mType.setImageResource(R.drawable.ic_done_orange_600_24dp);
                break;
            case 3:
            case 4:
                period = task.getDatePerformUsers();
                holder.mType.setImageResource(R.drawable.ic_close_green_600_24dp);
                break;
        }
        holder.mStatus.setVisibility(period < 0 ? View.VISIBLE : View.INVISIBLE);
        holder.mAuthor.setText(task.getEmployeeSurname() + " " + task.getEmployeeName() + "." + task.getEmployeeMiddleName() + ".");

    }

    @Override
    public int getItemCount() {
        return mTasks.size();
    }

    public void addTasks(List<Task> tasks) {
        mTasks.addAll(tasks);
        notifyDataSetChanged();
    }

    public void addTask(Task task) {
        mTasks.add(0, task);
        notifyItemInserted(0);
    }

    public void clear() {
        mTasks.clear();
        notifyDataSetChanged();
    }

    public class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.id)
        TextView mId;
        @BindView(R.id.text)
        TextView mText;
        @BindView(R.id.status)
        ImageView mStatus;
        @BindView(R.id.type)
        ImageView mType;
        @BindView(R.id.author)
        TextView mAuthor;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if (mCallback != null) {
                mCallback.onItemClick(mTasks.get(getAdapterPosition()));
            }
        }
    }
}
