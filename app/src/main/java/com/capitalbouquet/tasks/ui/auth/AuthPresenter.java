package com.capitalbouquet.tasks.ui.auth;


import com.capitalbouquet.tasks.data.model.auth.User;
import com.capitalbouquet.tasks.ui.base.BasePresenter;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class AuthPresenter extends BasePresenter<AuthView> {

    public void auth(String login, String password) {
        getMvpView().showProgress();
        mDataManager.auth(login, password)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        getMvpView()::showSuccess,
                        getMvpView()::showError
                );
    }

    public void tryAuth() {
        User user = mDataManager.getUser();
        if (user != null) {
            getMvpView().auth(user);
        }
    }
}
