package com.capitalbouquet.tasks.ui.main.tasks;


import com.capitalbouquet.tasks.ui.base.BasePresenter;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class TasksPresenter extends BasePresenter<TasksView> {

    public void getTasks(int type, int offset) {
        mDataManager.getTasks(type, offset)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        getMvpView()::showTasks,
                        getMvpView()::showError
                );
    }
}
