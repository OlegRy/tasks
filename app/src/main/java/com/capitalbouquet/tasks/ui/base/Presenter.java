package com.capitalbouquet.tasks.ui.base;


public interface Presenter<V extends MvpView> {

    void attachView(V view);
    void detachView();
}
