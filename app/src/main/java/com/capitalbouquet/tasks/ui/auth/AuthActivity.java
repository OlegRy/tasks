package com.capitalbouquet.tasks.ui.auth;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.AppCompatButton;
import android.support.v7.widget.AppCompatEditText;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.capitalbouquet.tasks.R;
import com.capitalbouquet.tasks.data.model.auth.AuthResponse;
import com.capitalbouquet.tasks.data.model.auth.User;
import com.capitalbouquet.tasks.ui.base.BaseActivity;
import com.capitalbouquet.tasks.ui.main.MainActivity;
import com.capitalbouquet.tasks.util.UIUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class AuthActivity extends BaseActivity implements AuthView {

    private MaterialDialog mProgress;
    private AuthPresenter mPresenter;
    private AuthViewHolder mViewHolder;

    public static void start(Context context) {
        context.startActivity(new Intent(context, AuthActivity.class));
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        initPresenter();
        mPresenter.tryAuth();
        setContentView(R.layout.activity_auth);
        initViewHolder();
    }

    @Override
    protected void onDestroy() {
        hideProgress();
        mPresenter.detachView();
        mPresenter = null;
        super.onDestroy();
    }

    @Override
    public void showSuccess(AuthResponse response) {
        hideProgress();
        if (response != null) {
            if (TextUtils.equals("error", response.getStatus())) {
                Toast.makeText(this, response.getValue(), Toast.LENGTH_LONG).show();
            } else {
                User user = response.getUser();
                if (user != null) {
                    auth(user);
                } else {
                    Toast.makeText(this, R.string.error, Toast.LENGTH_LONG).show();
                }
            }
        } else {
            Toast.makeText(this, R.string.error, Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void auth(User user) {
        MainActivity.start(this, user.getId(), user.getAvatar(), user.getName(), user.getSurname(), user.getMiddleName(), user.getEmail());
        finish();
    }

    @Override
    public void showProgress() {
        mProgress = UIUtil.showProgressDialog(this);
    }

    @Override
    public void hideProgress() {
        if (mProgress != null && mProgress.isShowing()) {
            mProgress.dismiss();
            mProgress = null;
        }
    }

    @Override
    public void showError(Throwable error) {
        hideProgress();
        Toast.makeText(this, R.string.error, Toast.LENGTH_LONG).show();
        error.printStackTrace();
    }

    private void initPresenter() {
        mPresenter = new AuthPresenter();
        mPresenter.attachView(this);
    }

    private void initViewHolder() {
        mViewHolder = new AuthViewHolder();
        initToolbar();
    }

    private void initToolbar() {
        setSupportActionBar(mViewHolder.mToolbar);
        setTitle(R.string.login_screen);
    }

    private void doAuth() {
        String login = mViewHolder.mLogin.getText().toString();
        String password = mViewHolder.mPassword.getText().toString();
        if (!TextUtils.isEmpty(login) && !TextUtils.isEmpty(password)) {
            mPresenter.auth(login, password);
        } else {
            Toast.makeText(this, R.string.login_and_password_cannot_be_empty, Toast.LENGTH_LONG).show();
        }
    }

    class AuthViewHolder {
        @BindView(R.id.toolbar)
        Toolbar mToolbar;
        @BindView(R.id.login)
        AppCompatEditText mLogin;
        @BindView(R.id.password)
        AppCompatEditText mPassword;
        @BindView(R.id.auth)
        AppCompatButton mAuth;

        public AuthViewHolder() {
            ButterKnife.bind(this, AuthActivity.this);
        }

        @OnClick(R.id.auth)
        void onAuthClick() {
            doAuth();
        }
    }
}
