package com.capitalbouquet.tasks.ui.main.tasks;


import com.capitalbouquet.tasks.R;

public class OutgoingTasksFragment extends TasksChildFragment {

    public static OutgoingTasksFragment newInstance(ProgressListener progressListener) {
        OutgoingTasksFragment fragment = new OutgoingTasksFragment();
        fragment.mProgressListener = progressListener;
        return fragment;
    }

    @Override
    protected int getPageTitle() {
        return R.string.outgoing;
    }

    @Override
    protected int getTaskType() {
        return 1;
    }
}
