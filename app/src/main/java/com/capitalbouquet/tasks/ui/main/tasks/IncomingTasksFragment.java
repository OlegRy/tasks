package com.capitalbouquet.tasks.ui.main.tasks;


import com.capitalbouquet.tasks.R;

public class IncomingTasksFragment extends TasksChildFragment {

    public static IncomingTasksFragment newInstance(ProgressListener progressListener) {
        IncomingTasksFragment fragment = new IncomingTasksFragment();
        fragment.mProgressListener = progressListener;
        return fragment;
    }

    @Override
    protected int getPageTitle() {
        return R.string.incoming;
    }

    @Override
    protected int getTaskType() {
        return 0;
    }
}
