package com.capitalbouquet.tasks.ui.main;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.TextView;

import com.afollestad.materialdialogs.MaterialDialog;
import com.bumptech.glide.Glide;
import com.capitalbouquet.tasks.BuildConfig;
import com.capitalbouquet.tasks.R;
import com.capitalbouquet.tasks.services.PusherService;
import com.capitalbouquet.tasks.ui.auth.AuthActivity;
import com.capitalbouquet.tasks.ui.base.BaseActivity;
import com.capitalbouquet.tasks.ui.main.tasks.TasksFragment;
import com.capitalbouquet.tasks.util.UIUtil;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

import static com.capitalbouquet.tasks.R.id.toolbar;

public class MainActivity extends BaseActivity implements MainView {

    private static final String EXTRA_AVATAR = "avatar";
    private static final String EXTRA_NAME = "name";
    private static final String EXTRA_SURNAME = "surname";
    private static final String EXTRA_MIDDLE_NAME = "middleName";
    private static final String EXTRA_EMAIL = "email";
    private static final String EXTRA_ID = "id";

    private MainViewHolder mViewHolder;
    private MaterialDialog mProgress;
    private MainPresenter mPresenter;

    private String mAvatar;
    private String mName;
    private String mSurname;
    private String mMiddleName;
    private String mEmail;
    private long mId;

    public static void start(Context context, long id, String avatar, String name, String surname, String middleName, String email) {
        Intent intent = new Intent(context, MainActivity.class);
        intent.putExtra(EXTRA_AVATAR, avatar);
        intent.putExtra(EXTRA_NAME, name);
        intent.putExtra(EXTRA_SURNAME, surname);
        intent.putExtra(EXTRA_MIDDLE_NAME, middleName);
        intent.putExtra(EXTRA_EMAIL, email);
        intent.putExtra(EXTRA_ID, id);
        context.startActivity(intent);
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initData();
        startPusher();
        initViewHolder();
        initPresenter();
        showFragment(TasksFragment.newInstance(R.string.my_tasks));
    }

    @Override
    protected void onDestroy() {
        hideProgress();
        mPresenter.detachView();
        mPresenter = null;
        super.onDestroy();
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() <= 1) {
            finish();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void logout() {
        hideProgress();
        AuthActivity.start(this);
        finish();
    }

    @Override
    public void showError(Throwable error) {
        hideProgress();
        error.printStackTrace();
    }

    @Override
    public void showProgress() {
        mProgress = UIUtil.showProgressDialog(this);
    }

    @Override
    public void hideProgress() {
        if (mProgress != null && mProgress.isShowing()) {
            mProgress.dismiss();
            mProgress = null;
        }
    }

    private void startPusher() {
        PusherService.start(this, mId);
    }

    private void initData() {
        Intent intent = getIntent();
        if (intent != null && intent.hasExtra(EXTRA_AVATAR)) {
            mId = intent.getLongExtra(EXTRA_ID, 0);
            mAvatar = intent.getStringExtra(EXTRA_AVATAR);
            mName = intent.getStringExtra(EXTRA_NAME);
            mSurname = intent.getStringExtra(EXTRA_SURNAME);
            mMiddleName = intent.getStringExtra(EXTRA_MIDDLE_NAME);
            mEmail = intent.getStringExtra(EXTRA_EMAIL);
        }
    }

    private void initViewHolder() {
        mViewHolder = new MainViewHolder();
        initToolbar();
        initNavigationDrawer();
    }

    private void initNavigationDrawer() {

        mViewHolder.mNavigationView.setNavigationItemSelectedListener(item -> {
            if (!item.isChecked()) {
                item.setChecked(true);
            }
            mViewHolder.mDrawerLayout.closeDrawers();
            return changeFragment(item.getItemId());
        });
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this ,mViewHolder.mDrawerLayout, mViewHolder.mToolbar,R.string.open_drawer, R.string.close_drawer){

            @Override
            public void onDrawerClosed(View drawerView) {
                super.onDrawerClosed(drawerView);
            }

            @Override
            public void onDrawerOpened(View drawerView) {

                super.onDrawerOpened(drawerView);
            }
        };
        mViewHolder.mDrawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
        initHeader();
    }

    private void initHeader() {
        View headerView = mViewHolder.mNavigationView.getHeaderView(0);
        Glide.with(this)
                .load(String.format("%s/assets/images/avatar/%s", BuildConfig.BASE_URL, mAvatar))
                .asBitmap()
                .centerCrop()
                .into((CircleImageView) headerView.findViewById(R.id.image));
        ((TextView) headerView.findViewById(R.id.name)).setText(String.format("%s %s %s", mSurname, mName, mMiddleName));
        ((TextView) headerView.findViewById(R.id.email)).setText(mEmail);
    }

    private boolean changeFragment(int itemId) {
        switch (itemId) {
            case R.id.my_tasks:
                showFragment(TasksFragment.newInstance(R.string.my_tasks));
                return true;
            case R.id.all_tasks:
                showFragment(TasksFragment.newInstance(R.string.all_tasks));
                return true;
            case R.id.logout:
                mPresenter.logout();
                return true;
        }
        return false;
    }

    private void showFragment(Fragment fragment) {
        getSupportFragmentManager()
                .beginTransaction()
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit();
    }

    private void initToolbar() {
        setSupportActionBar(mViewHolder.mToolbar);
    }

    private void initPresenter() {
        mPresenter = new MainPresenter();
        mPresenter.attachView(this);
    }

    class MainViewHolder {
        @BindView(R.id.navigation_view)
        NavigationView mNavigationView;
        @BindView(R.id.drawer_layout)
        DrawerLayout mDrawerLayout;
        @BindView(toolbar)
        Toolbar mToolbar;

        public MainViewHolder() {
            ButterKnife.bind(this, MainActivity.this);
        }
    }
}
