package com.capitalbouquet.tasks.ui.base;


import com.capitalbouquet.tasks.data.DataManager;

public abstract class BasePresenter<V extends MvpView> implements Presenter<V> {

    private V mMvpView;
    protected DataManager mDataManager;

    public BasePresenter() {
        mDataManager = DataManager.getInstance();
    }

    @Override
    public void attachView(V view) {
        mMvpView = view;
    }

    @Override
    public void detachView() {
        mMvpView = null;
    }

    public V getMvpView() {
        return mMvpView;
    }

    public boolean isViewAttached() {
        return mMvpView != null;
    }

    public void checkViewAttached() {
        if (!isViewAttached()) {
            throw new MvpViewNotAttachedException();
        }
    }

    public static class MvpViewNotAttachedException extends RuntimeException {
        public MvpViewNotAttachedException() {
            super("Please call Presenter.attachView(MvpView) before requesting data to the Presenter");
        }
    }
}
