package com.capitalbouquet.tasks.ui.base;


import android.support.annotation.StringRes;

public abstract class TitleFragment extends BaseFragment {

    @Override
    public void onStart() {
        super.onStart();
        getActivity().setTitle(getTitle());
    }

    @StringRes
    protected abstract int getTitle();
}
