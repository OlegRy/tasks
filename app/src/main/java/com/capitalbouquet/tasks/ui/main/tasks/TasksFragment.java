package com.capitalbouquet.tasks.ui.main.tasks;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.afollestad.materialdialogs.MaterialDialog;
import com.capitalbouquet.tasks.R;
import com.capitalbouquet.tasks.ui.base.TitleFragment;
import com.capitalbouquet.tasks.util.UIUtil;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TasksFragment extends TitleFragment implements TasksChildFragment.ProgressListener {

    private TasksViewHolder mViewHolder;
    private MaterialDialog mProgress;
    private TasksPagerAdapter mAdapter;
    private int mTitle;

    public static TasksFragment newInstance(int title) {
        TasksFragment fragment = new TasksFragment();
        fragment.mTitle = title;
        return fragment;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViewHolder(view);
    }

    private void initViewHolder(View view) {
        mViewHolder = new TasksViewHolder(view);
        initViewPager();
    }

    private void initViewPager() {
        mAdapter = new TasksPagerAdapter(getChildFragmentManager());
        mViewHolder.mPager.setAdapter(mAdapter);
        mViewHolder.mTabs.setupWithViewPager(mViewHolder.mPager);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_tasks;
    }

    @Override
    protected int getTitle() {
        return mTitle;
    }

    @Override
    public void showProgress() {
        if (mProgress == null || !mProgress.isShowing()) {
            mProgress = UIUtil.showProgressDialog(getContext());
        }
    }

    @Override
    public void hideProgress() {
        if (mProgress != null && mProgress.isShowing()) {
            mProgress.dismiss();
            mProgress = null;
        }
    }

    class TasksViewHolder {
        @BindView(R.id.tabs)
        TabLayout mTabs;
        @BindView(R.id.pager)
        ViewPager mPager;

        public TasksViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

    private class TasksPagerAdapter extends FragmentPagerAdapter {

        private List<TasksChildFragment> mFragments;

        public TasksPagerAdapter(FragmentManager fm) {
            super(fm);
            mFragments = new ArrayList<>();
            mFragments.add(IncomingTasksFragment.newInstance(TasksFragment.this));
            mFragments.add(OutgoingTasksFragment.newInstance(TasksFragment.this));
        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return getString(mFragments.get(position).getPageTitle());
        }
    }
}
