package com.capitalbouquet.tasks.ui.main.tasks;


import android.os.Bundle;
import android.support.annotation.IntDef;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import com.capitalbouquet.tasks.R;
import com.capitalbouquet.tasks.data.model.tasks.Task;
import com.capitalbouquet.tasks.ui.base.BaseFragment;
import com.capitalbouquet.tasks.ui.listeners.EndlessRecyclerOnScrollListener;
import com.capitalbouquet.tasks.ui.main.tasks.adapter.TaskAdapter;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class TasksChildFragment extends BaseFragment implements TasksView, TaskAdapter.OnItemClickListener {

    private TasksChildViewHolder mViewHolder;
    private TasksPresenter mPresenter;
    private TaskAdapter mAdapter;
    private int mOffset;
    protected ProgressListener mProgressListener;

    public interface ProgressListener {
        void showProgress();
        void hideProgress();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        initViewHolder(view);
        initPresenter();
        showProgress();
        mPresenter.getTasks(getTaskType(), mOffset);
    }

    @Override
    public void onDestroyView() {
        mPresenter.detachView();
        mPresenter = null;
        super.onDestroyView();
    }

    @StringRes
    protected abstract int getPageTitle();

    private void initPresenter() {
        mPresenter = new TasksPresenter();
        mPresenter.attachView(this);
    }

    private void initViewHolder(View view) {
        mViewHolder = new TasksChildViewHolder(view);
        LinearLayoutManager manager = new LinearLayoutManager(getContext());
        mAdapter = new TaskAdapter(null, this);
        mViewHolder.mTasks.setLayoutManager(manager);
        mViewHolder.mTasks.addOnScrollListener(new EndlessRecyclerOnScrollListener(manager) {
            @Override
            public void onLoadMore(int current_page) {
                mPresenter.getTasks(getTaskType(), mOffset);
            }
        });
        mViewHolder.mTasks.setAdapter(mAdapter);
        mViewHolder.mRefresher.setOnRefreshListener(() -> {
            mViewHolder.mRefresher.setRefreshing(true);
            mOffset = 0;
            mAdapter.clear();
            mPresenter.getTasks(getTaskType(), mOffset);
        });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_tasks_child;
    }

    @Override
    public void onItemClick(Task task) {

    }

    @Override
    public void showProgress() {
        if (mProgressListener != null) {
            mProgressListener.showProgress();
        }
    }

    @Override
    public void hideProgress() {
        if (mProgressListener != null) {
            mProgressListener.hideProgress();
        }
    }

    @Override
    public void showError(Throwable error) {
        mViewHolder.mRefresher.setRefreshing(false);
        hideProgress();
        error.printStackTrace();
    }

    @Override
    public void showTasks(List<Task> tasks) {
        mViewHolder.mRefresher.setRefreshing(false);
        hideProgress();
        if (mAdapter != null && tasks != null) {
            mAdapter.addTasks(tasks);
            mOffset += 20;
        }
    }

    @TaskType
    protected abstract int getTaskType();

    @IntDef({0, 1})
    public @interface TaskType {
    }

    class TasksChildViewHolder {

        @BindView(R.id.tasks)
        RecyclerView mTasks;
        @BindView(R.id.refresher)
        SwipeRefreshLayout mRefresher;

        public TasksChildViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }
}
