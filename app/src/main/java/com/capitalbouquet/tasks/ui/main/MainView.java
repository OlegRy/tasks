package com.capitalbouquet.tasks.ui.main;


import com.capitalbouquet.tasks.ui.base.ProgressMvpView;

public interface MainView extends ProgressMvpView {

    void logout();
}
