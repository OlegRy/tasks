package com.capitalbouquet.tasks.ui.main.tasks;


import com.capitalbouquet.tasks.data.model.tasks.Task;
import com.capitalbouquet.tasks.ui.base.ProgressMvpView;

import java.util.List;

public interface TasksView extends ProgressMvpView {

    void showTasks(List<Task> tasks);
}
