package com.capitalbouquet.tasks.ui.main;


import com.capitalbouquet.tasks.ui.base.BasePresenter;

import rx.android.schedulers.AndroidSchedulers;
import rx.schedulers.Schedulers;

public class MainPresenter extends BasePresenter<MainView> {

    public void logout() {
        mDataManager.clearToken()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    this::processResult
                );
    }

    private void processResult(boolean isCleared) {
        if (isCleared) {
            getMvpView().logout();
        }
    }
}
