package com.capitalbouquet.tasks.ui.base;


public interface ProgressMvpView extends MvpView {

    void showProgress();
    void hideProgress();
}
