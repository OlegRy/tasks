package com.capitalbouquet.tasks.ui.base;


public interface MvpView {

    void showError(Throwable error);
}
