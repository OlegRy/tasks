package com.capitalbouquet.tasks.ui.auth;


import com.capitalbouquet.tasks.data.model.auth.AuthResponse;
import com.capitalbouquet.tasks.data.model.auth.User;
import com.capitalbouquet.tasks.ui.base.ProgressMvpView;

public interface AuthView extends ProgressMvpView {

    void showSuccess(AuthResponse response);

    void auth(User user);
}
