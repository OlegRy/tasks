package com.capitalbouquet.tasks.services;


import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.os.IBinder;
import android.support.annotation.Nullable;

import com.capitalbouquet.tasks.data.model.notifications.Notification;
import com.capitalbouquet.tasks.util.NotificationUtil;
import com.google.gson.Gson;
import com.pusher.client.Pusher;
import com.pusher.client.channel.Channel;
import com.pusher.client.connection.ConnectionState;

public class PusherService extends Service {

    private static final String EXTRA_USER_ID = "userId";

    private Pusher mPusher;
    private long mUserId;

    public static void start(Context context, long userId) {
        Intent intent = new Intent(context, PusherService.class);
        intent.putExtra(EXTRA_USER_ID, userId);
        context.startService(intent);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mPusher = new Pusher("723fead032b7f6bc78d1");
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (mPusher != null && mPusher.getConnection().getState() != ConnectionState.CONNECTED) {
            mUserId = intent.getLongExtra(EXTRA_USER_ID, 0);
            Channel channel = mPusher.subscribe("notifications_android_user_" + mUserId);
            channel.bind("notification_android", (channelName, eventName, data) -> {
                Gson gson = new Gson();
                Notification notification = gson.fromJson(data, Notification.class);
                NotificationUtil.createNotification(getApplicationContext(), notification);
            });
            mPusher.connect();
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        if (mPusher != null && mPusher.getConnection().getState() == ConnectionState.CONNECTED) {
            mPusher.disconnect();
        }
        super.onDestroy();
    }
}
