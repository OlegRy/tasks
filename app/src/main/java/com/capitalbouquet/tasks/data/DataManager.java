package com.capitalbouquet.tasks.data;


import android.text.TextUtils;

import com.capitalbouquet.tasks.BuildConfig;
import com.capitalbouquet.tasks.data.local.DatabaseHelper;
import com.capitalbouquet.tasks.data.model.auth.AuthResponse;
import com.capitalbouquet.tasks.data.model.auth.User;
import com.capitalbouquet.tasks.data.model.tasks.Task;
import com.capitalbouquet.tasks.data.model.tasks.TasksResponse;
import com.capitalbouquet.tasks.data.remote.ApiService;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.List;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Observable;

public class DataManager {

    private static DataManager sInstance;

    private ApiService mApiService;
    private DatabaseHelper mDatabaseHelper;

    private DataManager() {
        Gson gson = new GsonBuilder()
                .setDateFormat("yyyy-MM-dd HH:mm:ss")
                .create();
        mApiService = new Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_URL)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build()
                .create(ApiService.class);
        mDatabaseHelper = DatabaseHelper.getInstace();
    }

    public static DataManager getInstance() {
        if (sInstance == null) {
            sInstance = new DataManager();
        }
        return sInstance;
    }

    public Observable<AuthResponse> auth(String login, String password) {
        return mApiService.auth(login, password).map(this::saveUser);
    }

    public Observable<List<Task>> getTasks(int type, int offset) {
        return Observable
                .fromCallable(() -> mDatabaseHelper.getUser())
                .map(User::getId)
                .flatMap(id -> mApiService.getTasks(id, type, offset))
                .map(TasksResponse::getTasks);
    }

    public User getUser() {
        return mDatabaseHelper.getUser();
    }

    public Observable<Boolean> clearToken() {
        return Observable.fromCallable(() -> {
            mDatabaseHelper.clearUser();
            return true;
        });
    }

    private AuthResponse saveUser(AuthResponse response) {
        if (response != null && TextUtils.equals("success", response.getStatus()) && response.getUser() != null) {
            User user = mDatabaseHelper.saveUser(response.getUser());
            response.setUser(user);
            return response;
        }
        return response;
    }
}
