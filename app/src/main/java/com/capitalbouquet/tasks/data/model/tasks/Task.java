package com.capitalbouquet.tasks.data.model.tasks;


import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Task {

    @SerializedName("status")
    private int status;

    @SerializedName("id")
    private long id;

    @SerializedName("task_id")
    private long taskId;

    @SerializedName("user_id")
    private long userId;

    @SerializedName("who_insert_id")
    private long whoInsertId;

    @SerializedName("change_content")
    private int changeContent;

    @SerializedName("change_content_out")
    private int changeContentOut;

    @SerializedName("declaim")
    private int declaim;

    @SerializedName("date_declaim")
    private Date dateDeclaim;

    @SerializedName("recall")
    private int recall;

    @SerializedName("date_recall")
    private Date dateRecall;

    @SerializedName("close_users")
    private int closeUsers;

    @SerializedName("multitask_full")
    private String multiTaskFull;

    @SerializedName("full")
    private String full;

    @SerializedName("date")
    private Date date;

    @SerializedName("date_begin")
    private Date dateBegin;

    @SerializedName("date_period")
    private Date datePeriod;

    @SerializedName("date_perform")
    private Date datePerform;

    @SerializedName("date_close")
    private Date dateClose;

    @SerializedName("image")
    private String image;

    @SerializedName("priority")
    private int priority;

    @SerializedName("points_id")
    private long pointsId;

    @SerializedName("select_responsible")
    private int selectResponsible;

    @SerializedName("trash")
    private int trash;

    @SerializedName("period")
    private int period;

    @SerializedName("date_perform_users")
    private int datePerformUsers;

    @SerializedName("employee_surname")
    private String employeeSurname;

    @SerializedName("employee_name")
    private String employeeName;

    @SerializedName("employee_middle_name")
    private String employeeMiddleName;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTaskId() {
        return taskId;
    }

    public void setTaskId(long taskId) {
        this.taskId = taskId;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getWhoInsertId() {
        return whoInsertId;
    }

    public void setWhoInsertId(long whoInsertId) {
        this.whoInsertId = whoInsertId;
    }

    public int getChangeContent() {
        return changeContent;
    }

    public void setChangeContent(int changeContent) {
        this.changeContent = changeContent;
    }

    public int getChangeContentOut() {
        return changeContentOut;
    }

    public void setChangeContentOut(int changeContentOut) {
        this.changeContentOut = changeContentOut;
    }

    public int getDeclaim() {
        return declaim;
    }

    public void setDeclaim(int declaim) {
        this.declaim = declaim;
    }

    public Date getDateDeclaim() {
        return dateDeclaim;
    }

    public void setDateDeclaim(Date dateDeclaim) {
        this.dateDeclaim = dateDeclaim;
    }

    public int getRecall() {
        return recall;
    }

    public void setRecall(int recall) {
        this.recall = recall;
    }

    public Date getDateRecall() {
        return dateRecall;
    }

    public void setDateRecall(Date dateRecall) {
        this.dateRecall = dateRecall;
    }

    public int getCloseUsers() {
        return closeUsers;
    }

    public void setCloseUsers(int closeUsers) {
        this.closeUsers = closeUsers;
    }

    public String getMultiTaskFull() {
        return multiTaskFull;
    }

    public void setMultiTaskFull(String multiTaskFull) {
        this.multiTaskFull = multiTaskFull;
    }

    public String getFull() {
        return full;
    }

    public void setFull(String full) {
        this.full = full;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public Date getDateBegin() {
        return dateBegin;
    }

    public void setDateBegin(Date dateBegin) {
        this.dateBegin = dateBegin;
    }

    public Date getDatePeriod() {
        return datePeriod;
    }

    public void setDatePeriod(Date datePeriod) {
        this.datePeriod = datePeriod;
    }

    public Date getDatePerform() {
        return datePerform;
    }

    public void setDatePerform(Date datePerform) {
        this.datePerform = datePerform;
    }

    public Date getDateClose() {
        return dateClose;
    }

    public void setDateClose(Date dateClose) {
        this.dateClose = dateClose;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getPriority() {
        return priority;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public long getPointsId() {
        return pointsId;
    }

    public void setPointsId(long pointsId) {
        this.pointsId = pointsId;
    }

    public int getSelectResponsible() {
        return selectResponsible;
    }

    public void setSelectResponsible(int selectResponsible) {
        this.selectResponsible = selectResponsible;
    }

    public int getTrash() {
        return trash;
    }

    public void setTrash(int trash) {
        this.trash = trash;
    }

    public int getPeriod() {
        return period;
    }

    public void setPeriod(int period) {
        this.period = period;
    }

    public int getDatePerformUsers() {
        return datePerformUsers;
    }

    public void setDatePerformUsers(int datePerformUsers) {
        this.datePerformUsers = datePerformUsers;
    }

    public String getEmployeeSurname() {
        return employeeSurname;
    }

    public void setEmployeeSurname(String employeeSurname) {
        this.employeeSurname = employeeSurname;
    }

    public String getEmployeeName() {
        return employeeName;
    }

    public void setEmployeeName(String employeeName) {
        this.employeeName = employeeName;
    }

    public String getEmployeeMiddleName() {
        return employeeMiddleName;
    }

    public void setEmployeeMiddleName(String employeeMiddleName) {
        this.employeeMiddleName = employeeMiddleName;
    }
}
