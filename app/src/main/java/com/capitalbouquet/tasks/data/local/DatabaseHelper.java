package com.capitalbouquet.tasks.data.local;


import com.capitalbouquet.tasks.data.model.auth.User;

import io.realm.Realm;

public class DatabaseHelper {

    private static DatabaseHelper sInstace;

    private DatabaseHelper() {
    }

    public static DatabaseHelper getInstace() {
        if (sInstace == null) {
            sInstace =  new DatabaseHelper();
        }
        return sInstace;
    }

    public User getUser() {
        Realm realm = Realm.getDefaultInstance();
        User realmData = realm.where(User.class).findFirst();
        User tmp = null;
        if (realmData != null) {
            tmp = realm.copyFromRealm(realmData);
        }
        realm.close();
        return tmp;
    }

    public void clearUser() {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        realm.where(User.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
        realm.close();
    }

    public User saveUser(User user) {
        Realm realm = Realm.getDefaultInstance();
        realm.beginTransaction();
        user = realm.copyToRealmOrUpdate(user);
        realm.commitTransaction();

        User tmp = realm.copyFromRealm(user);
        realm.close();
        return tmp;
    }
}
