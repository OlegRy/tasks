package com.capitalbouquet.tasks.data.model.base;


import android.support.annotation.StringDef;

import com.google.gson.annotations.SerializedName;

public class BaseResponse {

    @Status
    @SerializedName("status")
    private String status;
    @SerializedName("value")
    private String value;

    @Status
    public String getStatus() {
        return status;
    }

    public void setStatus(@Status String status) {
        this.status = status;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @StringDef(value = {
            "success",
            "error"
    })
    public @interface Status {
    }
}
