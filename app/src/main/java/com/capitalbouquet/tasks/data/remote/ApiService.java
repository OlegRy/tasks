package com.capitalbouquet.tasks.data.remote;


import com.capitalbouquet.tasks.data.model.auth.AuthResponse;
import com.capitalbouquet.tasks.data.model.tasks.TasksResponse;

import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import rx.Observable;

public interface ApiService {

    @FormUrlEncoded
    @POST("/api/auth_android/checkUser")
    Observable<AuthResponse> auth(@Field("login") String login, @Field("password") String password);

    @GET("/api/main_android/getTasks")
    Observable<TasksResponse> getTasks(@Query("user_id") long userId, @Query("type_task") int type, @Query("offset") int offset);
}
