package com.capitalbouquet.tasks.data.model.auth;


import com.capitalbouquet.tasks.data.model.base.BaseResponse;
import com.google.gson.annotations.SerializedName;

public class AuthResponse extends BaseResponse {

    @SerializedName("users")
    private User user;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
