package com.capitalbouquet.tasks.data.model.tasks;


import com.capitalbouquet.tasks.data.model.base.BaseResponse;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class TasksResponse extends BaseResponse {

    @SerializedName("result")
    private List<Task> tasks;

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}
