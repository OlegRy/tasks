package com.capitalbouquet.tasks.util;


import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.media.RingtoneManager;
import android.support.v4.app.NotificationCompat;

import com.capitalbouquet.tasks.R;
import com.capitalbouquet.tasks.data.model.notifications.Notification;
import com.capitalbouquet.tasks.receivers.NotificationDeleteReceiver;

public class NotificationUtil {

    private static NotificationCompat.InboxStyle sInboxStyle;
    private static int sMessagesCount = 0;

    public static void createNotification(Context context, Notification notification) {
        Intent deleteIntent = new Intent(context, NotificationDeleteReceiver.class);
        PendingIntent deletePendingIntent = PendingIntent.getBroadcast(context, 0, deleteIntent, 0);
        NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
        android.app.Notification pushNotification = builder.setContentTitle(notification.getTitle())
                .setContentText(sMessagesCount == 0 ? notification.getContent() : notification.getDescription())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setPriority(android.app.Notification.PRIORITY_HIGH)
                .setVibrate(new long[]{1000, 1000, 1000, 1000, 1000})
                .setGroup("group")
                .setGroupSummary(true)
                .setDeleteIntent(deletePendingIntent)
                .setStyle(getInboxStyle().addLine(notification.getContent()))
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
                .build();
        manager.notify(0, pushNotification);
        sMessagesCount++;
    }

    public static void updateInboxStyle() {
        sInboxStyle = new NotificationCompat.InboxStyle();
    }

    private static NotificationCompat.InboxStyle getInboxStyle() {
        if (sInboxStyle == null) {
            sInboxStyle = new NotificationCompat.InboxStyle();
        }
        return sInboxStyle;
    }

    public static void updateMessagesCount(int messagesCount) {
        sMessagesCount = messagesCount;
    }
}
