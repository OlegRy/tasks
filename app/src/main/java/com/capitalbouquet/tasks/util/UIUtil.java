package com.capitalbouquet.tasks.util;


import android.content.Context;

import com.afollestad.materialdialogs.MaterialDialog;
import com.capitalbouquet.tasks.R;

public class UIUtil {

    public static MaterialDialog showProgressDialog(Context context) {
        MaterialDialog.Builder builder = new MaterialDialog.Builder(context);
        return builder
                .title(R.string.loading)
                .content(R.string.please_wailt)
                .progress(true, 0)
                .cancelable(false)
                .show();
    }
}
